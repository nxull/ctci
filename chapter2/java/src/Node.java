public class Node<T> {
    Node next = null;
    T data;

    public Node(T d) {
        data = d;
    }

    public void append(T data) {
        Node end = new Node(data);
        Node n = this;
        while (n.next != null)
            n = n.next;
        n.next = end;
    }

    public Node remove(Node head, T data) {
        Node temp = head;
        if (temp.data == data)
            return temp.next;
        while (temp.next != null) {
            if (temp.next.data == data) {
                temp.next = temp.next.next;
                return head;
            }
            temp = temp.next;
        }
        return head;
    }

    public Node removeDuplicates(Node head){
        Node t = head;
        return t;
    }
}
